import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <div class="container bg-mg">
        <div class="row">
          <div class="col-sm-12 text-center">
            <div class="inner-logo">
              <img src="unifi-new-logo.png" class="img-responsive">
              </img>
            </div>
          </div>
        </div>

        <div class="row ptb">
          <div class="col-sm-12">
            <div class="inner-content">
              <h1>Hi Welcome,</h1>
              <h2><strong>UNIFI Rewards Portal</strong></h2>
            </div>
          </div>
        </div>

        <div class="row ptb">
          <div class="col-sm-4">
            <div class="inner-tile">
              <h4>34534 <small>Unifi</small></h4>
              <p class="grey">unifi in Bucket</p>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="inner-tile">
              <h4>1<small>ETH =</small> 3K Unifi</h4>
              <p class="grey">Price of UNIFI/xUNIFI</p>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="inner-tile">
              <h4>34534 <small>Unifi</small></h4>
              <p class="grey">Total xUNIFI</p>
            </div>
          </div>
        </div>

        <div class="row ptb">
          <div class="col-sm-6">
            <div class="inner-tile">
              <h4>1<small>xUnifi=</small>3% bucket</h4>
              <p class="grey">Percentage Ownership</p>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="inner-tile">
              <h4>1<small>xUNIFI</small> 5 UNIFI</h4>
              <p class="grey">Value of xUNIFI</p>
            </div>
          </div>
        </div>

        <div class="row ptb">
          <div class="col-sm-4">
            <div class="inner-tile2">
              <h4>Your xUNIFI</h4>
              <p class="grey">1 xUnifi = 3% of the bucket</p>

              <h4>Your UNIFI</h4>
              <p class="grey">23423</p>

              <h4>Your Ether</h4>
              <p class="grey">0.34</p>
            </div>
          </div>

          <div class="col-sm-8">
            <div class="inner-tile2">
              <h4>Buy or Redeem xUNIFI</h4>
              <p class="grey">Distributed among remaining staked $Unifi</p>

              <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter Amount to xUnifi">
                  
               </input>
                <a class="input-group-addon" id="basic-addon2">Buy xUNIFI</a>
                 </div>


                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Enter Amount to xUnifi" >
                    
                    </input>
                    <a class="input-group-addon" id="basic-addon2">Redeem xUNIFI</a>
                     </div>
                </div>
              </div>
            </div>
          </div>
    
      </div> 
  );
}

export default App;
